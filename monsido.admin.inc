<?php

/**
 * @file
 * Qualtrics admin conf form.
 */

/**
 * Callback function for hook_menu().
 */
function monsido_config_settings() {

  $form['monsido'] = array(
    '#type' => 'fieldset',
    '#title' => t('Monsido Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['monsido']['monsido_domain_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain token'),
    '#required' => TRUE,
    '#default_value' => variable_get('monsido_domain_token'),
    '#description' => t('Monsido domain token'),
  );

  $form['monsido']['monsido_js_scope'] = array(
    '#type' => 'textfield',
    '#title' => t('Drupal js scope'),
    '#required' => FALSE,
    '#default_value' => variable_get('monsido_js_scope'),
    '#description' => t('Leave it empty, Monsido js will be added to footer scope. Possible values are "header" or "footer"'),
  );

  return system_settings_form($form);
}
